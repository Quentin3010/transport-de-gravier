# Livraison de Gravier

Quentin BERNARD

## Description du projet

Le projet "Livraison de Gravier" a été conçu pour répondre aux besoins d'Ornicar, qui cherchait à maximiser les gains de vente en optimisant le chargement de gravier dans les compartiments de sa péniche. Réalisé au cours du semestre 5 en Modèle Linéaire de ma Licence Informatique, ce mini-projet a été l'occasion d'appliquer des concepts concrets à des problèmes du quotidien.

Le langage AMPL (A Mathematical Programming Language) a été choisi comme outil principal pour résoudre ce problème d'optimisation linéaire.

## Fonctionnalités
- Contraintes spécifiques :
	- Affectation des quantités minimales de chaque type de gravier déterminées par le client.
	- Respect des contraintes d'asthme d'Ornicar, excluant certains types de gravier dans des compartiments spécifiques.
	- Maintien de la stabilité de la péniche en garantissant que le centre de gravité reste entre 4.6m et 5.5m.
	- Imposition d'une limite de volume non utilisé dans les compartiments, ne dépassant pas 5m3.
	- ...
- Résolution du modèle : Application d'AMPL pour résoudre efficacement le problème d'optimisation.
- Bénéfice maximal : Calcul du chargement optimal des différents types de graviers dans les compartiments pour maximiser le bénéfice d'Ornicar.

## Mon rôle

J'ai pris en charge l'intégralité du projet, depuis la conception du modèle jusqu'à son implémentation en AMPL. Mon rôle a consisté à traduire les contraintes spécifiques définies par Ornicar en un modèle mathématique viable, et à résoudre ce modèle pour parvenir à une solution optimale. Mon travail a été réalisé de manière autonome, en suivant rigoureusement le cahier des charges pour garantir le succès du projet.


