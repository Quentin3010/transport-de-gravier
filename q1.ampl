# ampl
# model q1.ampl;

reset;
##############
# Question 1 #
##############

##########
# MODELE #
##########

#Chaque compartiment a sa propre hauteur, et a sa position déterminée par deux abscisses
#Chaque compartiment a également une masse maximale qu’il peut supporter
set COMPARTIMENT;
param abscisse_gauche {COMPARTIMENT} >= 0;
param abscisse_droite {COMPARTIMENT} >= 0;
param hauteur {COMPARTIMENT} >= 0;
param masse_maximale {COMPARTIMENT} >= 0;

#Les compartiments ont tous la même largeur de 4m
param largeur = 4;

#Volume de chacun des compartiments
param volume {c in COMPARTIMENT} = 
    largeur * (abscisse_droite [c] - abscisse_gauche [c]) * hauteur [c];

var volume_totale = sum {c in COMPARTIMENT} volume [c];

set GRAVIER;
param masse_volumique {GRAVIER} >= 0;
param prix_volumique {GRAVIER} >= 0;
param stock {GRAVIER} >= 0;
param masse_min_a_transporter {GRAVIER} >= 0;

#La combinaison qui interdit des types de gravier dans certains compartiment (0 = interdit / 1 = autorisée)
param interdit {COMPARTIMENT, GRAVIER} >= 0;

#Ornicar veut maximiser son bénéfice, qui est simplement égal à la valeur totale du gravier transporté.
#qte_transporte est un volume
var qte_transporte {COMPARTIMENT, g in GRAVIER} >= 0;

maximize profit :
    sum {c in COMPARTIMENT} sum {g in GRAVIER} prix_volumique [g] * qte_transporte [c,g] * interdit[c,g];

#Il y a un stock de gravier prédifinie
#stock est une masse
var masse_gravier_transporte {g in GRAVIER} = 
     sum {c in COMPARTIMENT} masse_volumique [g] * qte_transporte [c,g];

subject to quantite_max_gravier {g in GRAVIER} :
    masse_gravier_transporte [g] <= stock[g];

#Il y a une quantité minimale de chaque type de gravier à transporter
#stock est une masse
subject to quantite_min_gravier {g in GRAVIER} :
    masse_gravier_transporte [g] >= masse_min_a_transporter[g];

#Chaque container a un volume max à respecter
var volume_actuelle {c in COMPARTIMENT} =
    sum {g in GRAVIER} qte_transporte [c,g];

subject to volume_max {c in COMPARTIMENT} :
    volume_actuelle[c] <= volume[c];

#Chaque container a une masse max à respecter
var masse_actuelle {c in COMPARTIMENT} =
    sum {g in GRAVIER} (qte_transporte [c,g] * masse_volumique [g]);

subject to masse_max {c in COMPARTIMENT} :
    masse_actuelle[c] <= masse_maximale[c];

#Ornicar s’interdit de naviguer si le volume total non utilisé dans les compartiments dépasse 5m3
subject to quantite_min_totale : 
    volume_totale - (sum {c in COMPARTIMENT} volume_actuelle[c]) <= 5;

#Pour assurer la stabilité de la péniche, l’abscisse du centre de gravité de la péniche doit se situer entre 4.6 et 5.5m
var num = sum {c in COMPARTIMENT} (
    sum {g in GRAVIER} masse_actuelle [c]  * ((abscisse_gauche[c] + abscisse_droite[c]) / 2)
);

var den = sum {c in COMPARTIMENT} (
    sum {g in GRAVIER} masse_actuelle [c]
);

subject to test:
    den >= 1;

subject to contrainte_stabilite1 :
    num >= 4.6 * den;

subject to contrainte_stabilite2 :
    num <= 5.5 * den;




###########
# DONNEES #
###########

data;

#Sa péniche dispose de 4 compartiments pouvant stocker les graviers à transporter
param: COMPARTIMENT: abscisse_gauche  abscisse_droite  hauteur  masse_maximale :=
       A             1.0              3.5              1.8      30
       B             3.8              6.2              2.3      26
       C             6.5              7.3              2.9      24
       D             7.5              10.2             1.9      22;

#Il y a 6 types de graviers, qui ont chacun une masse volumique et un prix volumique
#Ornicar dispose d’un stock des différents graviers, et doit transporter pour chaque type de gravier une quantité minimale fixée par son client 
param: GRAVIER:   masse_volumique  prix_volumique  stock    masse_min_a_transporter :=
       subtil     2.4              35              18       15
       fin        1.7              28              25       13
       moyen      1.6              23              26       12
       grossier   1.2              20              30       17
       vulgaire   1.1              15              35       19
       caillou    0.9              13              10       5;

#Il ne veut pas qu’il y ait de gravier de type "subtil" dans les compartiments A et B
#Il ne veut pas qu’il y ait de gravier de type "fin" dans les compartiments A
param interdit :
    subtil  fin  moyen  grossier  vulgaire  caillou :=
A   0       0    1      1         1         1
B   0       1    1      1         1         1
C   1       1    1      1         1         1
D   1       1    1      1         1         1;




#########
# SOLVE #
#########

solve;

display masse_actuelle, masse_maximale, volume_actuelle, volume;

display qte_transporte;

display masse_min_a_transporter, masse_gravier_transporte, stock ;

display profit;